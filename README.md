![pipeline](https://gitlab.com/gitlab-org/ci-cd/package-stage/oci-conformance/test/badges/main/pipeline.svg?key_text=OCI%20Conformance%20&key_width=110)


This repository relies on the [OCI Distribution Conformance](https://github.com/opencontainers/distribution-spec/tree/main/conformance) tooling to trigger tests against the GitLab.com Container Registry using GitLab CI. [Artifacts reports](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html) are generated with the results from each test/pipeline execution.

The badge at the top of this README indicates whether conformance tests have passed or not based on the latest (daily) CI test pipeline.

Related to [Container Registry OCI Conformance (&10345)](https://gitlab.com/groups/gitlab-org/-/epics/10345).
